﻿#pragma once
#include <GL/glew.h>
#include <string>

#include <glm/mat4x4.hpp>

enum class ShaderType : GLenum
{
	FRAGMENT = GL_FRAGMENT_SHADER, 
	VERTEX   = GL_VERTEX_SHADER
};

class ShaderProgram
{
public:
	ShaderProgram(const std::string& vertexFile, const std::string& fragFile);
	virtual ~ShaderProgram();
	
	void start() const;
	void stop() const;

	void initialize();
	
protected:
	virtual void bindAttributes() = 0;
	virtual void getAllUniforms() = 0;

	void bindAttribute( GLuint attribute, const std::string& varName );
	GLuint getUniformLocation(const std::string& name);
	GLuint loadShader( const std::string& path, ShaderType type);

	void loadFloat( GLuint loc, float value);
	void loadVector( GLuint loc, const glm::vec3& value);
	void loadBool( GLuint loc, bool value);
	void loadMatrix( GLuint loc, const glm::mat4&  value);

private:
	bool initialized = false;

	GLuint programID;
	GLuint vertexID;
	GLuint fragmentID;
};
