﻿#pragma once
#include "Model.h"
#include "ModelTexture.h"

class TexturedModel
{
public:
	TexturedModel(Model model, ModelTexture texture);
	
	Model* getRawModel();
	ModelTexture* getTexture();


private:
	Model rawModel;
	ModelTexture texture;
};
