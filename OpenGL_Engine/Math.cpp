﻿#include "Math.h"

#include <glm/gtc/matrix_transform.hpp>


glm::mat4 Math::createTransformMatrix( const glm::vec3& translation, const glm::vec3& rotation, float scale)
{
	glm::mat4 transMatrix = glm::mat4(1.0f);
	
	transMatrix = glm::translate( transMatrix, translation);

	transMatrix = glm::rotate( transMatrix, glm::radians(rotation.x), {1, 0, 0} );
	transMatrix = glm::rotate( transMatrix, glm::radians(rotation.y), {0, 1, 0} );
	transMatrix = glm::rotate( transMatrix, glm::radians(rotation.z), {0, 0, 1} );

	transMatrix = glm::scale( transMatrix, {scale, scale, scale});

	return transMatrix;
}

glm::mat4 Math::createViewformMatrix( const Camera& camera )
{
	glm::mat4 viewMatrix = glm::mat4(1.0f);
	
	viewMatrix = glm::rotate( viewMatrix, glm::radians(camera.getPitch()), {1, 0, 0} );
	viewMatrix = glm::rotate( viewMatrix, glm::radians(camera.getYaw()), {0, 1, 0} );
	viewMatrix = glm::rotate( viewMatrix, glm::radians(camera.getRoll()), {0, 0, 1} );

	auto pos = camera.getPosition();
	pos *= -1;

	viewMatrix = glm::translate( viewMatrix, pos);

	return viewMatrix;
}
