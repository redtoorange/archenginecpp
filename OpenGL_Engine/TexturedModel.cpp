﻿#include "TexturedModel.h"

TexturedModel::TexturedModel( Model model, ModelTexture texture)
	:rawModel(model), texture(texture)
{
}

Model* TexturedModel::getRawModel()
{
	return &rawModel;
}

ModelTexture* TexturedModel::getTexture()
{
	return &texture;
}
