﻿#include "ModelLoader.h"
#include "Loader.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
using namespace Assimp;

Model ModelLoader::loadModel(const std::string& fileName, Loader& loader)
{
	Importer importer;
	const aiScene* scene = importer.ReadFile(fileName.c_str(), aiProcess_Triangulate | aiProcess_FlipUVs);
//	const aiScene* scene = importer.ReadFile(fileName.c_str(), 0);

	if( scene )
	{
		//Process the scene into meshes
		std::vector<aiMesh*> meshes;
		processNode(scene->mRootNode, scene, meshes);

		//TODO: Expend this to work on all meshes in the scene
		// Bit of a hack for now...
		aiMesh* stall = meshes[0];

		std::vector<float> verts;
		std::vector<float> coords;
		std::vector<float> normals;
		std::vector<int>   indices;
		
		// Unpack the mesh into a model
		for( unsigned int i = 0; i < stall->mNumVertices; i++)
		{
			verts.push_back(stall->mVertices[i].x);
			verts.push_back(stall->mVertices[i].y);
			verts.push_back(stall->mVertices[i].z);

			normals.push_back(stall->mNormals[i].x);
			normals.push_back(stall->mNormals[i].y);
			normals.push_back(stall->mNormals[i].z);
	
			if( stall->mTextureCoords[0])
			{
				coords.push_back(stall->mTextureCoords[0][i].x);
				coords.push_back(stall->mTextureCoords[0][i].y);
			}
		}

		std::cout << "NumFaces: " << stall->mNumFaces << std::endl;
		for(unsigned int i = 0; i < stall->mNumFaces; i++)
		{
			aiFace face = stall->mFaces[i];
			for(unsigned int j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}

		return loader.loadToVAO(verts, indices, coords, normals);
	}
	else
	{
		std::cout << "Failed to load the scene <" << fileName << ">..." << std::endl;
		std::cout << importer.GetErrorString() << std::endl;
	}

	return {};
}


void ModelLoader::processNode(aiNode* node, const aiScene* scene, std::vector<aiMesh*>& meshes)
{
	// process all the node's meshes (if any)
	for( unsigned int i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(mesh);
	}

	// then do the same for each of its children
    for(unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(node->mChildren[i], scene, meshes);
    }
}
