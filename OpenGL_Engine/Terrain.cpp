﻿#include "Terrain.h"

#include <vector>

#include "Loader.h"

Terrain::Terrain(int gridX, int gridY, Loader& loader, ModelTexture* texture)
	:x(gridX * TERRAIN_SIZE), z(gridY*TERRAIN_SIZE), texture(texture)
{
	generateTerrain(loader);
}

// Generate a flat terrain
void Terrain::generateTerrain( Loader& loader)
{
	int count = VERTEX_COUNT * VERTEX_COUNT;

	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<float> textureCoords;
	std::vector<int> indices( 6*(VERTEX_COUNT-1)*(VERTEX_COUNT-1) );


	for(int i=0;i<VERTEX_COUNT;i++){
		for(int j=0;j<VERTEX_COUNT;j++){
			float iFloat = i;
			float jFloat  = j;
			
			vertices.push_back(jFloat/ ((float)VERTEX_COUNT - 1) * TERRAIN_SIZE);
			vertices.push_back(0);
			vertices.push_back(iFloat/ ((float)VERTEX_COUNT - 1) * TERRAIN_SIZE);

			normals.push_back(0);
			normals.push_back(1);
			normals.push_back(0);
			
			textureCoords.push_back(jFloat/ ((float)VERTEX_COUNT - 1));
			textureCoords.push_back(iFloat/ ((float)VERTEX_COUNT - 1));
		}
	}

	int pointer = 0;
	for(int gz=0;gz<VERTEX_COUNT-1;gz++){
		for(int gx=0;gx<VERTEX_COUNT-1;gx++){
			int topLeft = (gz*VERTEX_COUNT)+gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz+1)*VERTEX_COUNT)+gx;
			int bottomRight = bottomLeft + 1;

			indices.push_back(topLeft);
			indices.push_back(bottomLeft);
			indices.push_back(topRight);

			indices.push_back(topRight);
			indices.push_back(bottomLeft);
			indices.push_back(bottomRight);
		}
	}

	model = loader.loadToVAO( vertices, indices, textureCoords, normals);
}


float Terrain::getX() const
{
	return x;
}

float Terrain::getZ() const
{
	return z;
}

Model* Terrain::getModel()
{
	return &model;
}

ModelTexture* Terrain::getTexture()
{
	return texture;
}


Rect Terrain::getBounds() const
{
	return Rect(x, z, TERRAIN_SIZE, TERRAIN_SIZE);
}
