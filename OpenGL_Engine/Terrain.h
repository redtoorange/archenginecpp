﻿#pragma once
#include "Model.h"
#include "ModelTexture.h"
#include "Rect.h"

class Loader;

const float TERRAIN_SIZE = 800;
const float VERTEX_COUNT = 128;

class Terrain
{
	

public:
	Terrain( int gridX, int gridY, Loader& loader, ModelTexture* texture);

	float getX() const;
	float getZ() const;

	Rect getBounds() const;

	Model* getModel();
	ModelTexture* getTexture();


private:

	float x;
	float z;

	Model model;
	ModelTexture* texture;

	void generateTerrain( Loader& loader);

	
};
