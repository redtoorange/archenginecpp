﻿#pragma once
#include <glm/glm.hpp>

class Camera
{
public:
	Camera( const glm::vec3& startPos = {0, 0, 0});

	void update( float delta = 1.0f );
	
	const glm::vec3& getPosition() const;

	float getPitch() const;
	float getRoll() const;
	float getYaw() const;

private:
	const float cameraSpeed = 10.0f;
	glm::vec3 position;
	
	float pitch = 0;
	float roll = 0;
	float yaw = 0;

	float mouseX;
	float mouseY;
	float sensitivity = 0.05f;
	bool firstMouse = true;
};
