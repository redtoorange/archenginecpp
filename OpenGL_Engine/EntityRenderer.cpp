﻿#include "EntityRenderer.h"


#include "Math.h"
#include <glm/gtc/matrix_transform.hpp>
#include "BatchRenderer.h"


// Old render that uses just some verts stored in a VAO
//void EntityRenderer::renderBatch(const Model& model)
//{
//	// bind VAO and enable vertex position attribute
//	glBindVertexArray( model.getVaoID());
//	glEnableVertexArrayAttrib(model.getVaoID(), 0);
//	
//	//	Draw the model using the element array
//	glDrawElements(GL_TRIANGLES, model.getVertexCount(), GL_UNSIGNED_INT, 0);
//	
//	//	unbind VAO and disable the attribute
//	glDisableVertexArrayAttrib(model.getVaoID(), 0);
//	glBindVertexArray(0);
//}

// Old renderBatch that using a texture and no transformation
//void EntityRenderer::renderBatch( TexturedModel& model)
//{
//	Model* m = model.getRawModel();
//
//	// bind VAO and enable vertex position attribute
//	glBindVertexArray(m->getVaoID());
//	glEnableVertexArrayAttrib(m->getVaoID(), 0);
//	glEnableVertexArrayAttrib(m->getVaoID(), 1);
//	
//	glActiveTexture(GL_TEXTURE0);
//	glBindTexture(GL_TEXTURE_2D, model.getTexture()->getID());
//
//	//	Draw the model using the element array
//	glDrawElements(GL_TRIANGLES, m->getVertexCount(), GL_UNSIGNED_INT, 0);
//	
//	//	unbind VAO and disable the attribute
//	glDisableVertexArrayAttrib(m->getVaoID(), 0);
//	glDisableVertexArrayAttrib(m->getVaoID(), 1);
//	glBindVertexArray(0);
//}


// New iteration of the entityRenderer that will renderBatch an entire model using a entityShader
//void EntityRenderer::renderBatch( Entity& entity, EntityShader& entityShader, bool useLighting)
//{
//	// Unpack the entity
//	TexturedModel* model = entity.getModel();
//
//	const GLuint tex = model->getTexture()->getID();
//	const GLuint vao = model->getRawModel()->getVaoID();
//	const GLuint verts = model->getRawModel()->getVertexCount();
//
//	// bind VAO and enable vertex position attribute
//	glBindVertexArray(vao);
//	glEnableVertexArrayAttrib(vao, 0);	// Positions
//	glEnableVertexArrayAttrib(vao, 1);	// Tex coords
//	if( useLighting) glEnableVertexArrayAttrib(vao, 2);	// Normals
//
//	// get the model's transform (translation, rot, scale) and apply them in the entityShader
//	const glm::mat4 transMatrix = Math::createTransformMatrix( entity.getPosition(), entity.getRotation(), entity.getScale());
//	entityShader.loadTransMatrix(transMatrix);
//
//	// Load the specular lighting for the texture
//	ModelTexture* texture = model->getTexture();
//	entityShader.loadShineVariables(texture->getShineDampening(), texture->getReflectivity());
//
//	// Cache and bind the model's texture for rendering
//	glActiveTexture(GL_TEXTURE0);
//	glBindTexture(GL_TEXTURE_2D, tex);
//
//	// Draw the model using the element array
//	glDrawElements(GL_TRIANGLES, verts, GL_UNSIGNED_INT, nullptr);
//	
//	// Cleanup
//	glDisableVertexArrayAttrib(vao, 0);
//	glDisableVertexArrayAttrib(vao, 1);
//	if( useLighting) glDisableVertexArrayAttrib(vao, 2);
//
//	glBindVertexArray(0);
//}


// Create a entityRenderer with a projection matrix.  Push the matrix into the entityShader
EntityRenderer::EntityRenderer( EntityShader* shader, const glm::mat4& projectionMatrix )
:	shader(shader)
{
	//	Must start and stop the entityShader to modify the uniforms...
	this->shader->start();
	this->shader->loadProjMatrix( projectionMatrix );
	this->shader->stop();
}


// Render each instance of a TexturedModel
void EntityRenderer::render(std::map<TexturedModel*, vector<ModelInstance*>>& entities)
{
	for( pair<TexturedModel*, vector<ModelInstance*>> p : entities)
	{
		const GLuint verts = p.first->getRawModel()->getVertexCount();
		prepareTexturedModel(p.first);

		BatchRenderer::setBackFaceCulling( !p.first->getTexture()->isTransparent() );

		// Render all of the instances
		for( ModelInstance* e : p.second)
		{
			// Adjust the entityShader for the specifi instance
			prepareInstance(e);
			glDrawElements(GL_TRIANGLES, verts, GL_UNSIGNED_INT, nullptr);
		}

		unbindTextureModel( p.first );
	}
}

// Prepare to render a specific TexturedModel
void EntityRenderer::prepareTexturedModel(TexturedModel* model)
{
	const GLuint tex = model->getTexture()->getID();
	const GLuint vao = model->getRawModel()->getVaoID();

	// bind VAO and enable vertex position attribute
	glBindVertexArray(vao);
	glEnableVertexArrayAttrib(vao, 0);	// Positions
	glEnableVertexArrayAttrib(vao, 1);	// Tex coords
	glEnableVertexArrayAttrib(vao, 2);	// Normals

	// Load the specular lighting for the texture
	ModelTexture* texture = model->getTexture();
	shader->loadShineVariables(texture->getShineDampening(), texture->getReflectivity());
	shader->loadFakeLighting(texture->hasFakeLighting() );

	// Cache and bind the model's texture for rendering
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);
}

// Unload a TexturedModel from openGL
void EntityRenderer::unbindTextureModel(TexturedModel* model)
{
	const GLuint vao = model->getRawModel()->getVaoID();

	// Cleanup
	glDisableVertexArrayAttrib(vao, 0);
	glDisableVertexArrayAttrib(vao, 1);
	glDisableVertexArrayAttrib(vao, 2);

	glBindVertexArray(0);
}

// Push an entities internal state to the Shader so it can be entityRenderer
void EntityRenderer::prepareInstance(ModelInstance* entity)
{
	// get the model's transform (translation, rot, scale) and apply them in the entityShader
	const glm::mat4 transMatrix = Math::createTransformMatrix( 
		entity->getPosition(), 
		entity->getRotation(), 
		entity->getScale());

	shader->loadTransMatrix(transMatrix);
}
