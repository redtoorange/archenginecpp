﻿#include "Model.h"


Model::Model()
	: vaoID(0), vertexCount(0)
{
}

Model::Model(GLuint vaoID, int vertexCount)
	: vaoID(vaoID), vertexCount(vertexCount)
{
}

GLuint Model::getVaoID() const
{
	return vaoID;
}

int Model::getVertexCount() const
{
	return vertexCount;
}

void Model::setVaoID( GLuint id  )
{
	vaoID = id;
}

void Model::setVertexCount( int count )
{
	vertexCount = count;
}
