﻿#pragma once
#include "TexturedModel.h"
#include <glm/glm.hpp>

class ModelInstance
{
public:
	ModelInstance();
	ModelInstance(TexturedModel* model, const glm::vec3& position, const glm::vec3& rotation, float scale);

	void translate( const glm::vec3& amount );
	void rotate( const glm::vec3& amount );

	TexturedModel* getModel() const;
	void setModel(TexturedModel* model);

	const glm::vec3& getPosition() const;
	void setPosition(const glm::vec3& pos);

	const glm::vec3& getRotation() const;
	void setRotation(const glm::vec3& rot);

	float getScale( ) const;
	void setScale( float scale );

private:
	TexturedModel* model = nullptr;

	glm::vec3 position;
	glm::vec3 rotation;
	float scale;
};
