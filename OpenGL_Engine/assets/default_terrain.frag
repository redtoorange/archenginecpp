#version 400 core

in vec2 pass_textureCoord;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;

out vec4 out_Color;

uniform sampler2D textureSampler;

// Diffuse Lighting
uniform vec3 lightColor;

// Specular Lighting
uniform float shineDamper;
uniform float reflectivity;

void main( void )
{
	vec3 unitNormal = normalize( surfaceNormal);
	vec3 unitToLightVec  = normalize( toLightVector );
	

	float nDot = dot(unitNormal, unitToLightVec);
	float brightness = max(nDot, 0.2);
	vec3 diffuse = brightness * lightColor;

	vec3 unitToCameraVec = normalize( toCameraVector );
	vec3 lightDirection  = -unitToLightVec;
	vec3 reflectedLightDir = reflect(lightDirection, unitNormal);

	float specularFactor = dot(reflectedLightDir, unitToCameraVec);
	specularFactor = max(specularFactor, 0.0);
	float dampedFactor = pow(specularFactor, shineDamper);
	vec3 finalSpecular = dampedFactor * reflectivity * lightColor;

	

	out_Color = vec4(diffuse, 1.0) * texture( textureSampler, pass_textureCoord) + vec4(finalSpecular, 1.0);
}