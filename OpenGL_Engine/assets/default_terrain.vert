#version 400 core

uniform mat4 transMatrix;
uniform mat4 projMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;

in vec3 position;
in vec2 textureCoord;
in vec3 normal;

out vec2 pass_textureCoord;
out vec3 surfaceNormal;
out vec3 toLightVector;
out vec3 toCameraVector;

void main( void )
{
	vec4 worldPosition = transMatrix * vec4( position, 1.0f);
	// Last item must be 1.0 to indicate a position, 0.0 indicates a direction
	gl_Position = projMatrix * viewMatrix * worldPosition;
	pass_textureCoord = textureCoord * 40;

	surfaceNormal = (transMatrix * vec4(normal, 0.0)).xyz;
	toLightVector = lightPosition - worldPosition.xyz;
	toCameraVector = (inverse(viewMatrix) * vec4(0, 0, 0, 1)).xyz - worldPosition.xyz;
}