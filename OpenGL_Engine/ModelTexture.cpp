﻿#include "ModelTexture.h"

ModelTexture::ModelTexture(GLuint id)
	: textureID(id)
{
}

GLuint ModelTexture::getID() const
{
	return textureID;
}

void ModelTexture::setShineDampening(float shineDamp)
{
	shineDampening = shineDamp;
}

void ModelTexture::setReflectivity(float reflect)
{
	reflectivity = reflect;
}

float ModelTexture::getShineDampening() const
{
	return shineDampening;
}

float ModelTexture::getReflectivity() const
{
	return reflectivity;
}

bool ModelTexture::isTransparent() const
{
	return hasTransparency;
}

void ModelTexture::setTransparent( bool has_transparency)
{
	hasTransparency = has_transparency;
}


bool ModelTexture::hasFakeLighting() const
{
	return useFakeLighting;
}

void ModelTexture::setFakeLighting(bool fakeLighting)
{
	useFakeLighting = fakeLighting;
}
