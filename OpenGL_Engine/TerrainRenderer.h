﻿#pragma once
#include "TerrainShader.h"

#include <vector>
#include "Terrain.h"

class TerrainRenderer
{
public:
	TerrainRenderer( TerrainShader* shader, const glm::mat4& projectionMatrix );

	void render(std::vector<Terrain*>& terrains);
private:
	TerrainShader* shader;

	void prepareTerrain(Terrain* terrain);
	void unloadTerrain(Terrain* terrain);
	void loadTransformMatrix(Terrain* terrain);
};
