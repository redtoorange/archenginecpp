﻿#include "ShaderProgram.h"

#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <fstream>

GLuint ShaderProgram::loadShader(const std::string& path, ShaderType type)
{
	std::ifstream ifs(path);
	std::string content( (std::istreambuf_iterator<char>(ifs) ),
						 (std::istreambuf_iterator<char>()    ) );

	GLint length = static_cast<GLint>(content.size());
	const char* shader = content.c_str();

	GLuint shaderID = glCreateShader(static_cast<GLenum>(type));
	glShaderSource( shaderID, 1, &shader, &length);
	glCompileShader( shaderID);

	GLint status;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);
	if( status == GL_FALSE )
	{
		GLsizei log_length = 0;
		GLchar info[500];

		glGetShaderInfoLog( shaderID, 500, &log_length, info);
		
		std::cout << "Failed to compile shader <" << path << ">" << std::endl;
		for( int i = 0; i < log_length; i++)
		{
			std::cout << info[i];
		}
	}

	return shaderID;
	
}


ShaderProgram::ShaderProgram(const std::string& vertexFile, const std::string& fragFile)
{
	std::cout << "Loading Shader..." << std::endl;

	std::cout << "\tCompiling Vertex Shader <" << vertexFile << ">" << std::endl;
	vertexID = loadShader( vertexFile, ShaderType::VERTEX);

	std::cout << "\tCompiling Fragment Shader <" << fragFile << ">" << std::endl;
	fragmentID = loadShader(fragFile, ShaderType::FRAGMENT);

	programID = glCreateProgram();
	std::cout << "\tProgram Created" << std::endl;
}

void ShaderProgram::start() const
{
	if( initialized )
		glUseProgram( programID);
	else
		std::cout << "Shader has not been fully initialized." << std::endl;
}

void ShaderProgram::stop() const
{
	glUseProgram(0);
}


ShaderProgram::~ShaderProgram()
{
	stop();

	glDetachShader(programID, vertexID);
	glDetachShader(programID, fragmentID);

	glDeleteShader( vertexID );
	glDeleteShader( fragmentID );

	glDeleteProgram( programID );
}

void ShaderProgram::bindAttribute(GLuint attribute, const std::string& varName)
{
	glBindAttribLocation(programID, attribute, varName.c_str());
}

// Binds all attributes and uniforms based on the derived implementation
void ShaderProgram::initialize()
{
	std::cout << "Binding..." << std::endl;
	glAttachShader( programID, vertexID);
	glAttachShader( programID, fragmentID);

	bindAttributes();

	glLinkProgram( programID );
	glValidateProgram( programID );

	getAllUniforms();

	initialized = true;

	std::cout << "Shader finished." << std::endl;
}

GLuint ShaderProgram::getUniformLocation(const std::string& name)
{
	return glGetUniformLocation( programID, name.c_str() );
}

void ShaderProgram::loadFloat(GLuint loc, float value)
{
	glUniform1f( loc, value );
}

void ShaderProgram::loadVector(GLuint loc, const glm::vec3& value)
{
	glUniform3f(loc, value.x, value.y, value.z);
}

void ShaderProgram::loadBool(GLuint loc, bool value)
{
	if( value )
		glUniform1f(loc, 1);
	else
		glUniform1f(loc, 0);	
}


void ShaderProgram::loadMatrix(GLuint loc, const glm::mat4& value)
{
	glUniformMatrix4fv( loc, 1, GL_FALSE, glm::value_ptr(value));
}
