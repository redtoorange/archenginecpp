﻿#pragma once
#include "Model.h"
#include <string>
#include <vector>
#include <iostream>

class Loader;
struct aiNode;
struct aiScene;
struct aiMesh;

class ModelLoader
{
public:
	// Load in a model and store it's data at a Model
	static Model loadModel( const std::string& fileName, Loader& loader );

private:
	// Process each node of the mesh and then process the node's children.  All meshes are stored in meshes.
	static void processNode( aiNode* node, const aiScene* scene, std::vector<aiMesh*>& meshes);
};
