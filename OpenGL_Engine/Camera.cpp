﻿#include "Camera.h"

#include <SFML/Graphics.hpp>


Camera::Camera(const glm::vec3& startPos)
	: position(startPos)
{
	if(firstMouse)
    {
		auto pos(sf::Mouse::getPosition());
        mouseX = pos.x;
        mouseY = pos.y;
        firstMouse = false;
    }
}

const glm::vec3& Camera::getPosition() const
{
	return position;
}

float Camera::getPitch() const
{
	return pitch;
}

float Camera::getRoll() const
{
	return roll;
}

float Camera::getYaw() const
{
	return yaw;
}

void Camera::update( float delta  )
{
	if( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
	{
		position.z -= cameraSpeed * delta;
	}
	else if( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
	{
		position.z += cameraSpeed * delta;
	}

	if( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
	{
		position.x += cameraSpeed * delta;
	}
	else if( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
	{
		position.x -= cameraSpeed * delta;
	}

	if( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R))
	{
		position.y += cameraSpeed * delta;
	}
	else if( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F))
	{
		position.y -= cameraSpeed * delta;
	}

	auto pos(sf::Mouse::getPosition());

	float offsetX = pos.x - mouseX;
	mouseX = pos.x;

	float offsety = pos.y - mouseY;
	mouseY = pos.y;

	offsetX *= sensitivity;
	offsety *= sensitivity;

	yaw   += offsetX;
    pitch += offsety;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;
}
