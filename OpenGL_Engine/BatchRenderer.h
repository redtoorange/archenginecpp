﻿#pragma once
#include "EntityShader.h"
#include "EntityRenderer.h"
#include <map>
#include <vector>
#include <memory>
#include "TerrainShader.h"
#include "TerrainRenderer.h"

using namespace std;


const float FOVY_ANGLE = 45.0f;
const float NEAR_PLANE = 0.1f;
const float FAR_PLANE = 1000.0f;


class BatchRenderer
{
public:
	BatchRenderer( float width = 800, float height = 600 );

	void createProjectionMatrix(float width, float height);
	void clearBuffer( const glm::vec4& color);
	void renderBatch( Light& light, Camera& camera);

	void batchEntity( ModelInstance* entity);
	void batchTerrain( Terrain* terrain);

	static void setBackFaceCulling( bool culling);

private:
	EntityShader entityShader;
	TerrainShader terrainShader;

	unique_ptr<EntityRenderer> entityRenderer;
	unique_ptr<TerrainRenderer> terrainRenderer;

	map<TexturedModel*, vector<ModelInstance*>> entities;
	vector<Terrain*> terrains;

	glm::mat4 projectionMatrix;
};
