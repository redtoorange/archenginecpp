﻿#pragma once

#include <GL/glew.h>

class ModelTexture
{
public:
	ModelTexture(GLuint id);

	GLuint getID() const;

	void setShineDampening( float shineDamp );
	void setReflectivity( float reflect );
	
	float getShineDampening( ) const;
	float getReflectivity( ) const;


	bool isTransparent() const;
	void setTransparent( bool has_transparency);

	bool hasFakeLighting() const;
	void setFakeLighting( bool fakeLighting);

private:
	GLuint textureID;

	float shineDampening = 1;
	float reflectivity = 0;

	bool hasTransparency = false;
	bool useFakeLighting = false;
};
