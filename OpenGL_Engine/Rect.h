﻿#pragma once

struct Rect
{
	Rect(int x = 0, int y = 0, int width = 0, int height = 0);

	int x;
	int y;
	int width;
	int height;
};
