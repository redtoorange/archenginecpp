﻿#pragma once

#include "ShaderProgram.h"
#include "Camera.h"
#include "Light.h"

const std::string MODEL_VERTEX_FILE = "assets/default_model.vert";
const std::string MODEL_FRAGMENT_FILE = "assets/default_model.frag";


class EntityShader : public ShaderProgram
{
	
public:
	EntityShader();
	
	void loadTransMatrix( const glm::mat4& matrix);
	void loadProjMatrix( const glm::mat4& matrix);
	void loadViewMatrix( const Camera& camera);
	void loadLight( const Light& light);
	void loadShineVariables( float shineDamp, float reflectivity);
	void loadFakeLighting( bool useFakeLighting );
	
	
protected:
	void bindAttributes() override;
	void getAllUniforms() override;

private:
	GLuint transformLocation;
	GLuint projectionLocation;
	GLuint viewLocation;
	
	GLuint lightPosLoc;
	GLuint lightColorLoc;

	GLuint shineDampLoc;
	GLuint reflectivityLoc;
	
	GLuint useFakeLightingLoc;
};