﻿#include "BatchRenderer.h"
#include <glm/gtc/matrix_transform.hpp>

// Clear the buffers and clearBuffer them for drawing
void BatchRenderer::clearBuffer( const glm::vec4& color )
{
//	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	glClearColor( color.r, color.g, color.b, color.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


BatchRenderer::BatchRenderer( float width, float height )
{
	
	// Cull faces that are not visible
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	createProjectionMatrix( width, height);

	entityRenderer = make_unique<EntityRenderer>( &entityShader, projectionMatrix );
	terrainRenderer = make_unique<TerrainRenderer>( &terrainShader, projectionMatrix );
}

// Render all entities currently in the batch using the specified camera and light
void BatchRenderer::renderBatch(Light& light, Camera& camera)
{
	clearBuffer( {0.3, 0.3, 0.3, 1} );

	entityShader.start();
	entityShader.loadLight(light);
	entityShader.loadViewMatrix(camera);
	entityRenderer->render(entities);
	entityShader.stop();

	entities.clear();

	terrainShader.start();
	terrainShader.loadLight(light);
	terrainShader.loadViewMatrix(camera);
	terrainRenderer->render(terrains);
	terrainShader.stop();

	terrains.clear();
}

// Add an entity to the batch
void BatchRenderer::batchEntity(ModelInstance* entity)
{
	auto list = entities.find(entity->getModel());

	if( list == entities.end())
	{
		entities.emplace( entity->getModel(), vector<ModelInstance*>{entity});
	}
	else
	{
		list->second.push_back(entity);
	}
}


// Create a perspective projection matrix
void BatchRenderer::createProjectionMatrix(float width, float height)
{
	projectionMatrix = glm::perspective( FOVY_ANGLE, width/height, NEAR_PLANE, FAR_PLANE);
}

// Add a terrain to be render as part of the next render batch draw call
void BatchRenderer::batchTerrain(Terrain* terrain)
{
	terrains.push_back(terrain);
}


void BatchRenderer::setBackFaceCulling(bool culling)
{
	if( culling )
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	else
		glDisable(GL_CULL_FACE);
}
