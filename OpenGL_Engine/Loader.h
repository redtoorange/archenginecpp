﻿#pragma once
#include "Model.h"

#include <SFML/Graphics.hpp>
#include <GL/glew.h>
#include <vector>
#include <memory>

class Loader
{
public:
	Model loadToVAO(const std::vector<float>& positions, const std::vector<int>& indices, const std::vector<float>& coords, const std::vector<float>& normals);
	GLuint loadTexture( const std::string& fileName);
	~Loader();

private:
	GLuint createVAO();
	void storeDataInAttributeList( int attrib, int coordSize, const std::vector<float>& data);
	void unbindVAO();
	void bindIndicesBuffer( const std::vector<int>& indices);

	std::vector<GLuint> vaos;
	std::vector<GLuint> vbos;

	//	Keep the texture alive for useage
	std::vector<std::unique_ptr<sf::Texture>> textures;
};
