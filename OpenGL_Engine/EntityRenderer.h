﻿#pragma once

#include "ModelInstance.h"
#include "EntityShader.h"
#include <map>
#include <vector>
using namespace std;

class EntityRenderer
{
public:

	EntityRenderer( EntityShader* shader, const glm::mat4& projectionMatrix );

	void render(std::map<TexturedModel*, vector<ModelInstance*>>& entities);
	
private:
	
	//void createProjectionMatrix( float width, float height);
	//glm::mat4 projectionMatrix;

	EntityShader* shader = nullptr;

	void prepareTexturedModel( TexturedModel* model);
	void unbindTextureModel( TexturedModel* model );
	void prepareInstance(ModelInstance* entity);
};
