﻿#pragma once
#include <glm/glm.hpp>
#include "Camera.h"
#include <vector>

class Math
{
public:
	// Create a transform matrix using it's core parts
	static glm::mat4 createTransformMatrix( const glm::vec3& translation, const glm::vec3& rotation, float scale);
	static glm::mat4 createViewformMatrix(const Camera& camera );
};

