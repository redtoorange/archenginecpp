﻿#include "TerrainShader.h"
#include "Math.h"
#include <iostream>



void TerrainShader::bindAttributes()
{
	bindAttribute(0, "position");
	bindAttribute(1, "textureCoord");
	bindAttribute(2, "normal");

	std::cout << "\tAttributes Bound" << std::endl;
}

void TerrainShader::getAllUniforms()
{
	transformLocation = getUniformLocation("transMatrix");
	projectionLocation = getUniformLocation("projMatrix");
	viewLocation = getUniformLocation("viewMatrix");

	lightPosLoc = getUniformLocation("lightPosition");
	lightColorLoc = getUniformLocation("lightColor");

	shineDampLoc = getUniformLocation("shineDamper");
	reflectivityLoc = getUniformLocation("reflectivity");

	std::cout << "\tUniforms Bound" << std::endl;
}

TerrainShader::TerrainShader()
	: ShaderProgram(TERRAIN_VERTEX_FILE, TERRAIN_FRAGMENT_FILE)
{
	initialize();
}


void TerrainShader::loadTransMatrix(const glm::mat4& matrix)
{
	loadMatrix(transformLocation, matrix);
}

void TerrainShader::loadProjMatrix(const glm::mat4& matrix)
{
	loadMatrix(projectionLocation, matrix);
}

void TerrainShader::loadViewMatrix(const Camera& camera)
{
	glm::mat4 viewMatrix = Math::createViewformMatrix(camera);
	loadMatrix(viewLocation, viewMatrix);
}

void TerrainShader::loadLight(const Light& light)
{
	loadVector(lightPosLoc, light.getPosition());
	loadVector(lightColorLoc, light.getColor());
}


void TerrainShader::loadShineVariables(float shineDamp, float reflectivity)
{
	loadFloat(shineDampLoc, shineDamp);
	loadFloat(reflectivityLoc, reflectivity);
}
