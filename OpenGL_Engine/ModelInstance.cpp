﻿#include "ModelInstance.h"

ModelInstance::ModelInstance()
	: position(0, 0, 0), rotation(0, 0, 0), scale(0)
{
}


ModelInstance::ModelInstance(TexturedModel* model, const glm::vec3& position, const glm::vec3& rotation, float scale)
		: model(model),
		  position(position),
		  rotation(rotation),
		  scale(scale)
	{
	}

TexturedModel* ModelInstance::getModel() const
{
	return model;
}

const glm::vec3& ModelInstance::getPosition() const
{
	return position;
}

const glm::vec3& ModelInstance::getRotation() const
{
	return rotation;
}


float ModelInstance::getScale() const
{
	return scale;
}

void ModelInstance::translate(const glm::vec3& amount)
{
	position.x += amount.x;
	position.y += amount.y;
	position.z += amount.z;
}

void ModelInstance::rotate(const glm::vec3& amount)
{
	rotation.x += amount.x;
	rotation.y += amount.y;
	rotation.z += amount.z;
}

void ModelInstance::setModel(TexturedModel* model)
{
	this->model = model;
}

void ModelInstance::setPosition(const glm::vec3& pos)
{
	position = pos;
}

void ModelInstance::setRotation(const glm::vec3& rot)
{
	rotation = rot;
}

void ModelInstance::setScale(float scale)
{
	this->scale = scale;
}
