﻿#pragma once

#include <GL/glew.h>

class Model
{
public:
	Model();
	Model( GLuint vaoID, int vertexCount);

	GLuint getVaoID() const;
	void setVaoID( GLuint id );

	int getVertexCount() const;
	void setVertexCount( int count );

private:
	GLuint vaoID;
	int vertexCount;
};
