﻿#pragma once

#include "ShaderProgram.h"
#include "Camera.h"
#include "Light.h"


const std::string TERRAIN_VERTEX_FILE = "assets/default_terrain.vert";
const std::string TERRAIN_FRAGMENT_FILE = "assets/default_terrain.frag";

class TerrainShader : public ShaderProgram
{
public:
	TerrainShader();
	void bindAttributes() override;

	void loadTransMatrix( const glm::mat4& matrix);
	void loadProjMatrix( const glm::mat4& matrix);
	void loadViewMatrix( const Camera& camera);
	void loadLight( const Light& light);
	void loadShineVariables( float shineDamp, float reflectivity);
	
	
	void getAllUniforms() override;

private:
	GLuint transformLocation;
	GLuint projectionLocation;
	GLuint viewLocation;
	
	GLuint lightPosLoc;
	GLuint lightColorLoc;

	GLuint shineDampLoc;
	GLuint reflectivityLoc;

};
