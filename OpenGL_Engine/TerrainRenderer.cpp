﻿#include "TerrainRenderer.h"

#include "Math.h"

TerrainRenderer::TerrainRenderer(TerrainShader* shader, const glm::mat4& projectionMatrix)
	: shader(shader)
{

	shader->start();
	shader->loadProjMatrix(projectionMatrix);
	shader->stop();
}


void TerrainRenderer::render(std::vector<Terrain*>& terrains)
{
	for( Terrain* t : terrains)
	{
		prepareTerrain(t);
		loadTransformMatrix(t);
		glDrawElements(GL_TRIANGLES, t->getModel()->getVertexCount(), GL_UNSIGNED_INT, nullptr);
		unloadTerrain(t);
	}
}


// Prepare to render a specific TexturedModel
void TerrainRenderer::prepareTerrain(Terrain* terrain)
{
	ModelTexture* texture = terrain->getTexture();
	const GLuint vao = terrain->getModel()->getVaoID();

	// bind VAO and enable vertex position attribute
	glBindVertexArray(vao);
	glEnableVertexArrayAttrib(vao, 0);	// Positions
	glEnableVertexArrayAttrib(vao, 1);	// Tex coords
	glEnableVertexArrayAttrib(vao, 2);	// Normals

	// Load the specular lighting for the texture
	shader->loadShineVariables(texture->getShineDampening(), texture->getReflectivity());

	// Cache and bind the model's texture for rendering
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->getID());
}

// Push an entities internal state to the Shader so it can be entityRenderer
void TerrainRenderer::loadTransformMatrix(Terrain* terrain)
{
	// get the model's transform (translation, rot, scale) and apply them in the entityShader
	const glm::mat4 transMatrix = Math::createTransformMatrix( 
		{terrain->getX(), -5, terrain->getZ()}, {0, 0, 0}, 1);

	shader->loadTransMatrix(transMatrix);
}

// Unload a TexturedModel from openGL
void TerrainRenderer::unloadTerrain(Terrain* terrain)
{
	const GLuint vao = terrain->getModel()->getVaoID();

	// Cleanup
	glDisableVertexArrayAttrib(vao, 0);
	glDisableVertexArrayAttrib(vao, 1);
	glDisableVertexArrayAttrib(vao, 2);

	glBindVertexArray(0);
}

