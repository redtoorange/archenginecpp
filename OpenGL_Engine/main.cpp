#include <SFML/Graphics.hpp>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <iostream>

#include "Loader.h"
#include "EntityShader.h"
#include "ModelTexture.h"
#include "TexturedModel.h"
#include "ModelInstance.h"
#include "ModelLoader.h"
#include "BatchRenderer.h"


const std::string WINDOW_TITLE = "ArchEngine Demo";
const unsigned int WINDOW_WIDTH = 1600;
const unsigned int WINDOW_HEIGHT = 900;


enum class State
{
	RUNNING, PAUSED, STOPPED
};
State gState = State::RUNNING;


// Compile using the SFML shader to help detect errors
bool compileShader( sf::Shader& shader)
{
	if( !shader.loadFromFile("assets/default_model.vert", sf::Shader::Vertex) )
	{
		std::cout << "Faile to load vertex entityShader" << std::endl;
		return GL_FALSE;
	}
	if( !shader.loadFromFile("assets/default_model.frag", sf::Shader::Fragment) )
	{
		std::cout << "Failed to load fragment entityShader" << std::endl;
		return GL_FALSE;
	}

	GLuint proj = glGetUniformLocation( shader.getNativeHandle(), "projMatrix");
	std::cout << "ProjMatrix: " << proj << std::endl;

	return GL_TRUE;
}

bool frameBound = true;

// Event handler
void handleEvents( sf::RenderWindow& window)
{
	sf::Event event;
	while( window.pollEvent(event))
	{
		if( event.type == event.Closed)
			gState = State::STOPPED;

		if( event.type == event.KeyPressed && event.key.code == sf::Keyboard::Key::Escape)
			gState = State::STOPPED;

		if( event.type == sf::Event::KeyReleased)
		{
			if( event.key.code == sf::Keyboard::Key::Num1)
			{
				std::cout << "Frame Limit: 60\n";
				window.setFramerateLimit(60);
			}
			else if(event.key.code == sf::Keyboard::Key::Num2)
			{
				std::cout << "Frame Limit: 144\n";
				window.setFramerateLimit(144);
			}
			else if(event.key.code == sf::Keyboard::Key::Num3)
			{
				std::cout << "Frame Limit: Unlimited\n";
				window.setFramerateLimit(0);
			}
			else if(event.key.code == sf::Keyboard::Key::Num4)
			{
				std::cout << "Entities unbound from frame rate\n";
				frameBound = !frameBound;
			}	
		}
		
	}
}

void populate( float density, Terrain* target, TexturedModel* source, vector<ModelInstance>& storage, float scale = 1.0f)
{
	// Load in a bunch of trees
	const Rect bounds(target->getBounds());
	cout << "x: " << bounds.x 
		 << "y: " << bounds.y 
		 << "w: " << bounds.width 
		 << "h: " << bounds.height 
		 << "\n";

	int count = 0;

	for( int x = 0; x < bounds.width; x++)
	{
		for( int y = 0; y < bounds.height; y++)
		{
			float chance = (  static_cast<float>(((rand() % 1000) + 1)) / 1000.0f);

			if( chance < density)
			{
				count++;
				std::cout << chance << "\n";
				storage.emplace_back( ModelInstance{source, glm::vec3( (bounds.x + x), -5, (bounds.y + y)), {0, 0, 0}, scale} );
			}
				
		}
	}
	std::cout << "Trees: " << count << "\n";
}



int main()
{
	// You must reenable the depth buffer for SFML windows using a context
	sf::ContextSettings context{24, 8, 0, 3, 3, };
	sf::RenderWindow window{{WINDOW_WIDTH, WINDOW_HEIGHT}, WINDOW_TITLE, sf::Style::Default, context};
	window.setFramerateLimit(60);

	// Prep the context
	glewInit();
	
	// Tell GL to test which triangles should renderBatch infront of which.
	//	Without this, triangles will renderBatch in a random order and over top of each other.
	glEnable(GL_DEPTH_TEST);

//	sf::Shader entityShader;
//	if( !compileShader(entityShader))
//	{
//		char a;
//		std::cin >> a;
//		return GL_FALSE;
//	}
//	
	// Load our assets
	Loader loader;

//	EntityShader static_shader;
//	EntityRenderer entityRenderer( static_cast<float>(WINDOW_WIDTH), static_cast<float>(WINDOW_HEIGHT), static_shader);
//	TexturedModel stallModel{ ModelLoader::loadModel("assets/stall.obj", loader), loader.loadTexture("assets/stallTexture.png")};

	/*	// Test models to ensure proper rendering
	TexturedModel whiteDragon{ ModelLoader::loadModel("assets/dragon.obj", loader), loader.loadTexture("assets/dragonWhite.png")};
	TexturedModel redDragon{ ModelLoader::loadModel("assets/dragon.obj", loader), loader.loadTexture("assets/dragonRed.png")};
	TexturedModel wolf{ ModelLoader::loadModel("assets/wolf.obj", loader), loader.loadTexture("assets/wolf_edited.png")};

	ModelTexture* wTexture = whiteDragon.getTexture();
	wTexture->setReflectivity(1);
	wTexture->setShineDampening(5);

	ModelTexture* rTexture = redDragon.getTexture();
	rTexture->setReflectivity(1);
	rTexture->setShineDampening(5);

	ModelTexture* wolfTexture = wolf.getTexture();
	wolfTexture->setShineDampening(5);
	wolfTexture->setReflectivity(5);

	Entity wd{&whiteDragon, {-5, -5, -25}, {0, 0, 0}, 1};
	Entity rd{&redDragon, {5, -5, -25}, {0, 0, 0}, 1};
	Entity we{&wolf, {5, -5, -10}, {0, 0, 0}, 1};
	*/

	TexturedModel lowPolyTree{ ModelLoader::loadModel("assets/environment/lowPolyTree.obj", loader), loader.loadTexture("assets/environment/lowPolyTree.png")};
	
	TexturedModel tree{ ModelLoader::loadModel("assets/environment/tree.obj", loader), loader.loadTexture("assets/environment/tree.png")};
	
	TexturedModel fern{ ModelLoader::loadModel("assets/environment/fern.obj", loader), loader.loadTexture("assets/environment/fern.png")};
	fern.getTexture()->setTransparent(true);
	

	TexturedModel grassModel{ ModelLoader::loadModel("assets/environment/grassModel.obj", loader), loader.loadTexture("assets/environment/grassTexture.png")};
	grassModel.getTexture()->setTransparent(true);
	grassModel.getTexture()->setFakeLighting(true);

	ModelInstance wd{&lowPolyTree, {-10, -5, -25}, {0, 0, 0}, 0.5};
	ModelInstance rd{&tree, {-5, -5, -25}, {0, 0, 0}, 5};
	ModelInstance we{&fern, {0, -5, -25}, {0, 0, 0}, 1};
	ModelInstance g{&grassModel, {10, -5, -25}, {0, 0, 0}, 1};


	vector<ModelInstance> modelInstances;

	Camera camera;
	Light light{{0, 10, -20}, {1, 1, 1}};

	ModelTexture grass{loader.loadTexture("assets/grass.png")};

	Terrain terrain1{-1, -1, loader, &grass};
	Terrain terrain2{0, -1, loader, &grass};

	BatchRenderer renderer(WINDOW_WIDTH, WINDOW_HEIGHT);
	
	

	modelInstances.push_back(wd);
	modelInstances.push_back(rd);
	modelInstances.push_back(we);
	modelInstances.push_back(g);

	populate( 0.002f, &terrain1, &tree, modelInstances, 5 );
	populate( 0.002f, &terrain2, &tree, modelInstances, 5 );
	

	sf::Clock clock;
	float delta = 0;
	const float speed = 100;
	float time = 0.0f;
	int frames = 0;

	while( gState == State::RUNNING)
	{
		delta = clock.restart().asSeconds();

		time += delta;
		frames++;

		if( time > 1.0f)
		{
			time -= 1.0f;
			std::cout << "FPS: " <<  std::to_string(frames) << "\n";
			frames = 0;
		}

		handleEvents( window );

		// Update
//		for( ModelInstance* e : modelInstances)
//		{
//			if( frameBound )
//				e->rotate({0, speed * delta, 0});
//			else
//				e->rotate({0, speed, 0});
//		}
		

		camera.update( delta );


		// Render
		for( auto& e : modelInstances)
		{
			renderer.batchEntity(&e);
		}

		renderer.batchTerrain(&terrain1);
		renderer.batchTerrain(&terrain2);

		renderer.renderBatch( light, camera);

		window.display();
	}

	return 0;
}