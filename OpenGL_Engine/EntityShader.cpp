﻿#include "EntityShader.h"

#include "Math.h"
#include <iostream>

void EntityShader::bindAttributes()
{
	bindAttribute(0, "position");
	bindAttribute(1, "textureCoord");
	bindAttribute(2, "normal");

	std::cout << "\tAttributes Bound" << std::endl;
}

void EntityShader::getAllUniforms()
{
	transformLocation = getUniformLocation("transMatrix");
	projectionLocation = getUniformLocation("projMatrix");
	viewLocation = getUniformLocation("viewMatrix");

	lightPosLoc = getUniformLocation("lightPosition");
	lightColorLoc = getUniformLocation("lightColor");

	shineDampLoc = getUniformLocation("shineDamper");
	reflectivityLoc = getUniformLocation("reflectivity");

	useFakeLightingLoc = getUniformLocation("useFakeLighting");

	std::cout << "\tUniforms Bound" << std::endl;
}

EntityShader::EntityShader()
	: ShaderProgram(MODEL_VERTEX_FILE, MODEL_FRAGMENT_FILE)
{
	initialize();
}


void EntityShader::loadTransMatrix(const glm::mat4& matrix)
{
	loadMatrix(transformLocation, matrix);
}

void EntityShader::loadProjMatrix(const glm::mat4& matrix)
{
	loadMatrix(projectionLocation, matrix);
}

void EntityShader::loadViewMatrix(const Camera& camera)
{
	glm::mat4 viewMatrix = Math::createViewformMatrix(camera);
	loadMatrix(viewLocation, viewMatrix);
}

void EntityShader::loadLight(const Light& light)
{
	loadVector(lightPosLoc, light.getPosition());
	loadVector(lightColorLoc, light.getColor());
}


void EntityShader::loadShineVariables(float shineDamp, float reflectivity)
{
	loadFloat(shineDampLoc, shineDamp);
	loadFloat(reflectivityLoc, reflectivity);
}

void EntityShader::loadFakeLighting(bool useFakeLighting)
{
	loadBool(useFakeLightingLoc, useFakeLighting);
}
