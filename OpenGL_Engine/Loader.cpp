﻿#include "Loader.h"


Model Loader::loadToVAO(
	const std::vector<float>& positions, 
	const std::vector<int>& indices,
	const std::vector<float>& coords,
	const std::vector<float>& normals
)
{
	GLuint vaoID = createVAO();

	bindIndicesBuffer( indices );

	storeDataInAttributeList(0, 3, positions);
	storeDataInAttributeList(1, 2, coords);
	storeDataInAttributeList(2, 3, normals);

	unbindVAO();
	return Model{vaoID, static_cast<int>(indices.size())};
}

GLuint Loader::createVAO()
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	vaos.push_back(vaoID);
	glBindVertexArray(vaoID);
	
	return vaoID;
}

void Loader::storeDataInAttributeList(int attrib, int coordSize, const std::vector<float>& data)
{
	//	Create the VBO
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);

	//	Make VBO current and push the data into it
	glBindBuffer( GL_ARRAY_BUFFER, vboID);
	glBufferData( GL_ARRAY_BUFFER, sizeof(float) * data.size(), data.data(), GL_STATIC_DRAW);

	//	Attach the VAO's attribute list pointer to this VBO then unbind
	glVertexAttribPointer( attrib, coordSize, GL_FLOAT, false, 0, NULL);
	glBindBuffer( GL_ARRAY_BUFFER, 0);

}

void Loader::unbindVAO()
{
	glBindVertexArray(0);
}


void Loader::bindIndicesBuffer(const std::vector<int>& indices)
{
	//	Create the VBO
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);

	//	Make the buffer current then push the data into it
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * indices.size(), indices.data(), GL_STATIC_DRAW);

	// DO NOT UNBIND THE ELEMENT ARRAY
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

Loader::~Loader()
{
	//	Cleanup all VAOs and VBOs
	for( auto& vao : vaos)
		glDeleteVertexArrays( 1, &vao );

	for( auto& vbo : vbos)
		glDeleteBuffers( 1, &vbo );
}

GLuint Loader::loadTexture(const std::string& fileName)
{
	std::unique_ptr<sf::Texture> texture = std::make_unique<sf::Texture>();

	texture->loadFromFile( fileName );
	texture->setRepeated(true);
	GLuint textID = texture->getNativeHandle();

	textures.push_back(std::move(texture));
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
//	
	return textID;
}
