## Arch Engine
Lightweight 3D engine that uses modern OpenGL.  Designed to help bootstrap a new OpenGL project providing streamlined access to core features, while remaining flexible and extensible.

### Dependencies
* [Assimp]( https://github.com/assimp/assimp) – Asset loader
* [Glew]( https://github.com/nigels-com/glew) – OpenGL Extension Wrangler
* [Glm]( https://github.com/g-truc/glm) – OpenGL Math Library
* [SFML]( https://github.com/SFML/SFML) – Windowing and system access.
